/*
** sapin.c for sapin in /home/thing-_a/rendu/Piscine-C-sapin
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Wed Oct  2 22:20:22 2013 a
** Last update Sun Oct  6 16:17:55 2013 a
*/

int	get_size_max(int size)
{
  int	nb_max;
  int	coef;
  int	i;

  nb_max = 1;
  i = 0;
  coef = 6;
  while (i < size)
    {
      if (i != 0 && i % 2 == 0)
	coef = coef + 2;
      nb_max = nb_max + coef;
      i = i + 1;
    }
  return (nb_max);
}

void	my_print_line(int size_max, int nb_star, int trunc)
{
  int	i;
  char	pict;
  int	nb_space;

  i = 0;
  pict = '*';
  nb_space = ((size_max - 1) / 2) - (nb_star / 2);
  if (trunc > 0)
    pict = '|';
  while (i < nb_space)
    {
      write(1, " ", 1);
      i = i + 1;
    }
  i = 0;
  while (i < nb_star)
    {
      write(1, &pict, 1);
      i = i + 1;
    }
  write(1, "\n", 1);
}

void	print_leaves(int nb_line_max, int size_max, int size)
{
  int	i;
  int	nb_star;
  int	to_show;
  int	count;
  int	space;

  i = 0;
  nb_star = 1;
  to_show = 4;
  count = to_show;
  space = 2;
  while (i < nb_line_max)
    {
      if (i == to_show)
	{
	  if (count != 4 && count % 2 == 0)
	    space = space + 2;
	  to_show = to_show + count + 1;
	  nb_star = nb_star - 2 - space;
	  count = count + 1;
	}
      my_print_line(size_max, nb_star, 0);
      nb_star = nb_star + 2;
      i = i + 1;
    }
}

void	print_trunc(int size_max, int size)
{
  int	nb_trunc;
  int	i;

  nb_trunc = size;
  i = 0;
  if (size % 2 == 0)
    nb_trunc = nb_trunc + 1;
  while (i < size)
    {
      my_print_line(size_max, nb_trunc, 1);
      i = i + 1;
    }
}

void	sapin(int taille)
{
  int	nb_line_max;
  int	size_max;
  int	count;

  if (taille > 0)
    {
      count = taille;
      nb_line_max = 0;
      while (count > 0)
	{
	  nb_line_max = nb_line_max + (count + 3);
	  count = count - 1;
	}
      size_max = get_size_max(taille);
      print_leaves(nb_line_max, size_max, taille);
      print_trunc(size_max, taille);
    }
  else
    write(2, "The argument must be positive.\n", 31);
}
